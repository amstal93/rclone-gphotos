FROM debian:stretch
RUN apt-get update && apt-get install -y --no-install-recommends wget ca-certificates && update-ca-certificates && wget https://beta.rclone.org/rclone-beta-latest-linux-amd64.deb && dpkg -i rclone-beta-latest-linux-amd64.deb && rm rclone-beta-latest-linux-amd64.deb && apt-get purge -y wget && rm -rf /var/lib/apt/lists/*
RUN useradd -Um user
RUN chown -R user. /home/user
USER user
RUN mkdir -p /home/user/.config/rclone
COPY docker-entrypoint.sh /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]
